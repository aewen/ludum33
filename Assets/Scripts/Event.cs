﻿using UnityEngine;
using System.Collections;

public class Event : MonoBehaviour {

	public GameObject eventWindowOK;
	public GameObject eventWindowDecision;
	private GameObject AudioManager;

	//"ok" means that you just click ok to dismiss the box
	//"choice" means that you have to pick one of two things to happen
	public enum EventType
	{
		randomOK, randomChoice, triggerOK, triggerChoice, NULL
	};
	
	public abstract class GameEvent
	{
		public EventType eventType;
		public string displayText;
		public bool active = false;		// default is false. Only use if the event is ongoing.
		public int currentMonth = -1;	// store the current month of the year
		
		// all events must implement all of these functions
		// if the event doesn't use them implement them with no body
		public abstract void executeWithContinent(Continent affectedContinent);
		public abstract void executeWithoutContinent();
		public abstract void executeOptionA();
		public abstract void executeOptionB();
		public abstract void ongoingEvent();
		
		//--- EVENT IMPLEMENTATIONS BELOW --- //
		
		// NEGATIVE MONEY
		
		public class MoneyBelowZero : GameEvent
		{
			public MoneyBelowZero()
			{
				eventType = EventType.triggerOK;
				displayText = "You are now in debt! You will lose shareholder confidence every month until your money goes above $0";
			}
			public override void executeWithoutContinent()
			{
				// nothing needs to happen
			}
			public override void ongoingEvent()
			{
				if(TimeManager.TIME_ELAPSED_IN_MONTHS != currentMonth)
				{
					Company.companyShareholderOpinion-=1;
				}
				currentMonth = TimeManager.TIME_ELAPSED_IN_MONTHS;
			}
			public override void executeWithContinent(Continent affectedContinent){}
			public override void executeOptionA(){}
			public override void executeOptionB(){}
		}
		
		// WAR BREAKS OUT
		public class WarBreakout : GameEvent
		{
			public WarBreakout()
			{
				eventType = EventType.randomOK;
			}
			public override void executeWithContinent (Continent affectedContinent)
			{
				displayText = "A war has broken out in " + affectedContinent.name + "! You will lose half of all production in this region until the war ends.";
				for(int i = 0; i < affectedContinent.factories.Count; i++)
				{
					// half production from this factory
					Company.companyMoneyIN -= affectedContinent.factories[i].productionProfit;

					affectedContinent.factories[i].productionProfit = affectedContinent.factories[i].productionProfit/2;

					Company.companyMoneyIN += affectedContinent.factories[i].productionProfit;

				}
			}
			public override void executeWithoutContinent(){}
			public override void executeOptionA(){}
			public override void executeOptionB(){}
			public override void ongoingEvent(){}
		}
		
		// WAR BREAKS OUT
		public class WarOver : GameEvent
		{
			public WarOver()
			{
				eventType = EventType.randomOK;
			}
			public override void executeWithContinent (Continent affectedContinent)
			{
				displayText = "The war in " + affectedContinent.name + " has ended. Production has returned to normal.";
				for(int i = 0; i < affectedContinent.factories.Count; i++)
				{
					Company.companyMoneyIN -= affectedContinent.factories[i].productionProfit;

					// return production to normal
					affectedContinent.factories[i].productionProfit = affectedContinent.factories[i].productionProfit*2;
					Company.companyMoneyIN += affectedContinent.factories[i].productionProfit;
				}
			}
			public override void executeWithoutContinent(){}
			public override void executeOptionA(){}
			public override void executeOptionB(){}
			public override void ongoingEvent(){}
		}
		
		public class TestOptionEvent : GameEvent
		{
			public TestOptionEvent()
			{
				eventType = EventType.triggerChoice;
				displayText = "Option test event!!";
			}
			public override void executeOptionA ()
			{
				Company.companyMoney = 333;
			}
			public override void executeOptionB ()
			{
				Company.companyMoney = 777;
			}
			public override void executeWithoutContinent(){}
			public override void executeWithContinent(Continent affectedContinent){}
			public override void ongoingEvent(){}
		}

		// COMPANY HAS BEEN IDLE FOR TOO LONG
		public class IdleCompanyEvent : GameEvent
		{
			public IdleCompanyEvent()
			{
				eventType = EventType.triggerChoice;

				displayText = 	"\nIdle company, please take action." + 
						"\n<b>Option A:</b> I understand, give me one year to improve the situation. (Shareholder Opinion. V 5% & Event in 1 year.)" + 
						"\n<b>Option B:</b> I believe it's going well as it is. (Shareholder Opinion. V 20%)";

			}

			public override void executeOptionA ()
			{	
				Company.companyShareholderOpinion -= 5;
			}
			
			public override void executeOptionB ()
			{
				Company.companyShareholderOpinion -= 20;
			}

			public override void executeWithContinent (Continent affectedContinent){}
			public override void executeWithoutContinent(){}
			public override void ongoingEvent(){}
		}

		//PURCHASE NEW FACTORY
		public class PurchaseNewFactoryEvent: GameEvent
		{
			private Continent affectedContinent;
			private float basePrice, maintenanceCost, productionProfit;
			private string primaryResource;
			public PurchaseNewFactoryEvent(Continent _affectedContinent, int resourceNumber)
			{
				affectedContinent = _affectedContinent;
				
				if (resourceNumber == 0)
					primaryResource = affectedContinent.resourceOne;
				else if (resourceNumber == 1)
					primaryResource = affectedContinent.resourceTwo;
				else
					primaryResource = affectedContinent.resourceThree;
				

				eventType = EventType.randomChoice;

				displayText = "A recent report has shown that the company could potentially increase it's <b>income</b> and <b>Shareholder Opinion</b> at the same time by branching out into new markets in" + affectedContinent.name + 
					". What do you say?" + "\n Option A: This sounds interesting, I shall invest in a new venture. (Buy 1 Factory. Shareholder Opinion. ^ 5%)" + 
					"\n Option B: We can't purchase a factory right now. (Shareholder opinion ^ -5%)";
			}
			
			public override void executeOptionA ()
			{	
				
				basePrice = affectedContinent.CalculateFactoryPrice(Company.companyBaseFactoryPurchasePrice, primaryResource);
				maintenanceCost = affectedContinent.calculateFactoryMaintenanceCost(basePrice);
				productionProfit = affectedContinent.calculateFactoryProductionProfit(basePrice);				
			
				affectedContinent.PurchaseFactory((int)basePrice, (int)maintenanceCost, (int)productionProfit, primaryResource);
				Company.companyShareholderOpinion += 5;
			}
			
			public override void executeOptionB ()
			{
				Company.companyShareholderOpinion -= 5;
			}
			
			public override void executeWithoutContinent(){}
			public override void executeWithContinent(Continent affectedContinent){}
			public override void ongoingEvent(){}

		}
		
		public class WorkerProtestEvent: GameEvent
		{
			private int factoryNumber;
			
			public WorkerProtestEvent(int _factoryNumber)
			{
				factoryNumber = _factoryNumber;
				eventType = EventType.randomOK;
			}
		
			public override void executeWithContinent (Continent affectedContinent)
			{
				int cost = affectedContinent.factories[factoryNumber].productionProfit/5;
				displayText = "The workers in one of your factories have gone on strike due to the increased working hours. " + 
					"\n <b>Effect: Lose </b>" + cost.ToString("n0");
					
				Company.companyMoney -= cost;				
			}
			
			public override void executeWithoutContinent(){}
			public override void ongoingEvent(){}
			public override void executeOptionA (){}
			public override void executeOptionB (){}
		}
		
		public class InternationalInvestigationEvent : GameEvent
		{
			public InternationalInvestigationEvent(string affectedContinent)
			{
				eventType = EventType.randomChoice;
				displayText = "An investigation into your company's operations in " + affectedContinent + "has uncovered apalling working conditions for your employees."
					+ " You can choose to: \n <b>Option A:</b> Supress the report with bribes. Lose $2,000,000. Shareholder happiness is unaffected."
						+ "\n <b>Option B:</b> Allow the report to be published. Lose no money, but shareholder happiness will be reduced by 30%.";
			}
			
			public override void executeWithContinent(Continent affectedContinent)
			{
			}
			
			public override void executeOptionA ()
			{
				Company.companyMoney -= 2000000;
			}
			public override void executeOptionB()
			{
				Company.companyShareholderOpinion -= 35;
			}
			
			public override void executeWithoutContinent()
			{
			}
			
			public override void ongoingEvent(){}
		}
		
		public class EnvironmentalDisasterEvent : GameEvent
		{
			private Continent affectedContinent;
		
			public EnvironmentalDisasterEvent(Continent _affectedContinent)
			{
				affectedContinent = _affectedContinent;
				eventType = EventType.randomChoice;
				displayText = "There has been an environmental disaster at one of your factories in " + affectedContinent.name + 
							  ". \nOption A: Close the factory and pay to mend the damages (destroys factory and lose 1,000,000)." + 
							  "\nOption B: Attempt to silence the press on the issue (lose 20 public opinion).";
			}
			
			public override void executeOptionA ()
			{
				affectedContinent.DeleteFactory(affectedContinent.factories[0]);
				Company.companyMoney -= 1000000;
			}
			public override void executeOptionB()
			{
				Company.companyShareholderOpinion -= 20;
			}			
			
			public override void executeWithContinent(Continent affectedContinent){}			
			public override void executeWithoutContinent(){}
			public override void ongoingEvent(){}
		}
		
		public class BankruptEvent : GameEvent
		{
			public BankruptEvent()
			{
				eventType = EventType.triggerChoice;
				displayText = "You have been in debt for too long! Your investors have lost confidence and you have lost control of your factories and company." +
						"\n<b>GAME OVER!</b>\nOption A: Return to Main Menu. \nOption B: Restart the game.";
			}
			public override void executeOptionA ()
			{
				Application.LoadLevel ("MainMenu");
			}
			public override void executeOptionB()
			{
				Application.LoadLevel ("Map");
			}			
			
			public override void executeWithContinent(Continent affectedContinent){}			
			public override void executeWithoutContinent(){}
			public override void ongoingEvent(){}
		}
		
		public class ShareholdersAbandonEvent : GameEvent
		{
			public ShareholdersAbandonEvent()
			{
				eventType = EventType.triggerChoice;
				displayText = "Your shareholder approval rating has dropped below 0%, and your shareholders have revolted and removed you as head of the company." +
					"\n<b>GAME OVER!</b>\nOptionA: Return to Main Menu. \nOption B: Restart the game.";
			}
			public override void executeOptionA ()
			{
				Application.LoadLevel ("MainMenu");
			}
			public override void executeOptionB()
			{
				Application.LoadLevel ("Map");
			}			
			
			public override void executeWithContinent(Continent affectedContinent){}			
			public override void executeWithoutContinent(){}
			public override void ongoingEvent(){}
		}
		
		public class WorkerSuicideEvent : GameEvent
		{
			private Continent affectedContinent;
			private int factoryNumber;
		
			public WorkerSuicideEvent(Continent _continent, int _factoryNumber)
			{
				affectedContinent = _continent;
				factoryNumber = _factoryNumber;
				
				eventType = EventType.randomChoice;
				displayText = "Due to the poor conditions at your factory in " + affectedContinent.name + ", some workers have started" + 
				               "to commit suicide." + "\nOption A: Reduce work hours and increase pay to help them (work hours decrease by 2 hours, pay goes up $500" + 
				               "\nOption B: Install preventative measures to stop them (lose $500,000 and 10% shareholder opinion).";
			}
			public override void executeOptionA ()
			{
				affectedContinent.factories[factoryNumber].AdjustFactorySettings(-2, 500);
			}
			public override void executeOptionB()
			{
				Company.companyMoney -= 500000;
				Company.companyShareholderOpinion -= 10;
			}	
						
			public override void executeWithContinent(Continent affectedContinent){}			
			public override void executeWithoutContinent(){}
			public override void ongoingEvent(){}			
		}
	}

	void Start () {

	}
	
	// activates an event
	// overloaded function for when you want an event to affect a specific continent,
	// or the entire game
	public void FireEvent(GameEvent firedEvent, Continent cont)
	{
		if (AudioManager == null)
		{
			AudioManager = GameObject.Find("Audio");				
		}
	
		//execute the code for the event
		firedEvent.executeWithContinent(cont);
		eventWindowOK = GameObject.Find("EventWindow");
		eventWindowOK.GetComponent<UIEventWindow>().DisplayPanel();
		eventWindowOK.GetComponent<UIEventWindow>().SetText(firedEvent.displayText);
		AudioManager.GetComponent<AudioManager>().playSoundAlert();
	}
	
	
	
	public void FireEvent(GameEvent firedEvent)
	{
		if (AudioManager == null)
		{
			AudioManager = GameObject.Find("Audio");				
		}
		firedEvent.executeWithoutContinent();
		eventWindowOK = GameObject.Find("EventWindow");
		eventWindowOK.GetComponent<UIEventWindow>().DisplayPanel();
		eventWindowOK.GetComponent<UIEventWindow>().SetText(firedEvent.displayText);
		AudioManager.GetComponent<AudioManager>().playSoundAlert();
	}
	
	public void FireOptionalEvent(GameEvent firedEvent, Continent cont)
	{
		if (AudioManager == null)
		{
			AudioManager = GameObject.Find("Audio");				
		}
		// UI CODE
		eventWindowDecision = GameObject.Find("EventWindowDecision");
		eventWindowDecision.GetComponent<UIEventWindowDecision>().SetFiredEvent(firedEvent);
		eventWindowDecision.GetComponent<UIEventWindowDecision>().DisplayPanel();
		eventWindowDecision.GetComponent<UIEventWindowDecision>().SetText(firedEvent.displayText);
		AudioManager.GetComponent<AudioManager>().playSoundAlert();
		
	}
	
	public void FireOptionalEvent(GameEvent firedEvent)
	{
		// UI CODE
		eventWindowDecision = GameObject.Find("EventWindowDecision");
		eventWindowDecision.GetComponent<UIEventWindowDecision>().SetFiredEvent(firedEvent);
		eventWindowDecision.GetComponent<UIEventWindowDecision>().DisplayPanel();
		eventWindowDecision.GetComponent<UIEventWindowDecision>().SetText(firedEvent.displayText);
		
	}
	
	public void SetEventActive(GameEvent firedEvent, bool setActive)
	{
		firedEvent.active = setActive;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
