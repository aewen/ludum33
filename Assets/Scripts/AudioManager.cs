﻿using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour {

	public AudioSource soundPurchaseFactory;
	public AudioSource soundAlert;

	// Use this for initialization
	void Start () {
		//soundPurchaseFactory = gameObject.AddComponent<AudioSource>();
		//soundPurchaseFactory.clip = Resources.Load("Audio/Buy Factory") as AudioClip;
		//soundPurchaseFactory.Play ();
		//soundAlert.Play();
		//soundAlert = gameObject.AddComponent<AudioSource>();
		//soundAlert.clip = Resources.Load("Audio/Alert Sound") as AudioClip;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void playSoundPurchase()
	{
		//Debug.Log("Playing purchase sound");
		soundPurchaseFactory.Play();
	}
	
	public void playSoundAlert()
	{
		soundAlert.Play();
	}
}
