﻿using UnityEngine;
using System.Collections;

public class Factory : MonoBehaviour {

	int workerCount = 0;
	public int maintenanceCost = 0;
	public int productionProfit = 0;
	public int workHours = 0;
	public int wagePerMonth = 0;
	public int opinion = 0;
	public string factoryResource = "ERROR";
	public string contWealth = "Poor";

	// Use this for initialization
	void Start () 
	{
		workHours = 8;
	}
	
	// Update is called once per frame
	void Update ()
	{	
		switch(contWealth)
		{
		case "Impoverished": 	if(wagePerMonth / workHours < 470)
									opinion = -2;
								else if(wagePerMonth / workHours < 705)
									opinion = -1;
								else if(wagePerMonth / workHours < 1175)
									opinion = 0;
								else if(wagePerMonth / workHours < 1410)
									opinion = 1;
								else
									opinion = 2;
								break;

		case "Poor": 	if(wagePerMonth / workHours < 937)
							opinion = -2;
						else if(wagePerMonth / workHours < 1500)
							opinion = -1;
						else if(wagePerMonth / workHours < 2437)
							opinion = 0;
						else if(wagePerMonth / workHours < 3374)
							opinion = 1;
						else
							opinion = 2;
						break;

		case "Average": 	if(wagePerMonth / workHours < 1875)
							opinion = -2;
						else if(wagePerMonth / workHours < 3187)
							opinion = -1;
						else if(wagePerMonth / workHours < 5062)
							opinion = 0;
						else if(wagePerMonth / workHours < 5999)
							opinion = 1;
						else
							opinion = 2;
						break;

		case "Wealthy": 	if(wagePerMonth / workHours < 3750)
								opinion = -2;
							else if(wagePerMonth / workHours < 6750)
								opinion = -1;
							else if(wagePerMonth / workHours < 10500)
								opinion = 0;
							else if(wagePerMonth / workHours < 12375)
								opinion = 1;
							else
								opinion = 2;
							break;

		default:				opinion = 0; break;
		}
	}

	public void UpdateOpinions()
	{
		Company.companyShareholderOpinion += 2 * opinion;
	}

	public void AdjustFactorySettings(int hours, int wage)
	{
		// allow for adjusting of the factory settings
		Company.companyMoneyIN -= productionProfit;
		Company.companyMoneyOUT -= maintenanceCost;

		productionProfit -= workHours * 1500;

		workHours += hours;
		if(workHours > 24)
			workHours = 24;
		if(workHours < 1)
			workHours = 1;

		productionProfit += workHours * 1500;

		maintenanceCost -= wagePerMonth;

		wagePerMonth += wage;
		if(wagePerMonth < 1)
			wagePerMonth = 1;

		maintenanceCost += wagePerMonth;

		Company.companyMoneyIN += productionProfit;
		Company.companyMoneyOUT += maintenanceCost;


	}
	
	public void GenerateFactoryData(int count, int maint, int produc, string res, string wealthiness)
	{
		workerCount = count;
		maintenanceCost = maint;
		productionProfit = produc;
		factoryResource = res;
		contWealth = wealthiness;

		switch(contWealth)
		{
			case "Impoverished":	wagePerMonth = 7500; break;
			case "Poor":			wagePerMonth = 15000; break;
			case "Average":			wagePerMonth = 30000; break;
			case "Wealthy":			wagePerMonth = 60000; break;
			default:				wagePerMonth = 10000; break;
		}
	}
}
