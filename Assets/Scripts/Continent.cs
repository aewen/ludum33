﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Continent : MonoBehaviour {

	public GameObject landPrefab;
	public GameObject factoryPrefab;
	
	// statistics about this continent
	public int population;
	public string name;
	public string wealthiness;
	public string resourceOne, resourceTwo, resourceThree;
	public string language;
	public string cities;
	int area;
	float percentControlled;
	
	// list of factories on this continent
	public List<Factory> factories = new List<Factory>();
	
	int lastMonth = 0;
	
	public bool atWar;
	private bool isSelected, isHover;
	private float hoverInterp, selectInterp;
	public float SelectInterp{ get { return selectInterp; } }
	private static Color color = new Color(0.15f, 0.55f, 0.15f);
	private static Color warColor = new Color(0.55f, 0.15f, 0.15f);
	private static Color highlightAmount = new Color(0.1f, 0.1f, 0.1f);
	
	enum resources
	{
		Oil, Coal, Diamond, Copper, Iron, Coffee, Solar, Tea, Electronics, Wood
	};
	
	enum wealth
	{
		Impoverished, Poor, Average, Wealthy
	};
	
	enum languages
	{
		Carblenian, Donkleese, Adsontongue, Kokish
	};
	
	enum names
	{
		Carble, Donkli, Adson, Merycalto, Toak, Luda, Mantarok, Beeba, Ereklao, Maddcarin, Fareskol, Toprominikin, Asedal, Bububu, Pople, Maska, Tiermeknalos, Kok
	};

	public List<GameObject> landSections = new List<GameObject>();

	// Use this for initialization
	void Start () {
		
		CreateContinentInformation();

	}

	public void SetSelected(bool _Selected)
	{
		isSelected = _Selected;
	}

	public bool GetSelected()
	{
		return isSelected;
	}
	
	public void SetHover(bool _Hover)
	{
		isHover = _Hover;
	}
	
	public bool GetHover()
	{
		return isHover;
	}
	

	// Update is called once per frame
	void Update () {
		Color newColor = new Color();
		
		var position = transform.position;
		if(isSelected) 
		{
			selectInterp += Time.deltaTime * 3.0f;
			position.z = -1.0f;
		} else 
		{
			selectInterp -= Time.deltaTime * 3.0f;
			position.z = 0.0f;
		}
		selectInterp = Mathf.Clamp(selectInterp, 0.0f, 1.0f);
		float selectScale = Mathf.Lerp (1.0f, 2.5f, selectInterp);
		transform.localScale = new Vector3(selectScale, selectScale, selectScale);
		transform.position = position;
		
		if (isHover)
		{
			hoverInterp += Time.deltaTime * 3.0f;
		}
		else
		{
			hoverInterp -= Time.deltaTime * 3.0f;
		}
		hoverInterp = Mathf.Clamp(hoverInterp, 0.0f, 1.0f);
		
		if(atWar)
		{
			newColor = Color32.Lerp(warColor, warColor + highlightAmount, hoverInterp);
		}
		else
		{
			newColor = Color32.Lerp(color, color + highlightAmount, hoverInterp);
		}
		
		
		
		foreach(var land in landSections)
		{
			var sprite = land.GetComponent<SpriteRenderer>();
			
			sprite.color = newColor;
		}	
		
		// deduct maintenance costs every month
		// add profit every month
		if(lastMonth != TimeManager.TIME_ELAPSED_IN_MONTHS)
		{
		Debug.Log (factories.Count);
			for(int i = 0; i < factories.Count; i++)
			{
				Company.companyMoney -= factories[i].maintenanceCost;
				Company.companyMoney += factories[i].productionProfit;

				factories[i].UpdateOpinions();
			}
		}
		lastMonth = TimeManager.TIME_ELAPSED_IN_MONTHS;
	}

	public void RedrawLand()
	{
		if (landSections != null) 
		{
			for(int i = 0; i < landSections.Count; i++)
			{
				Destroy(landSections[i]);
			}

			landSections.Clear ();
		}

		for (int i = 0; i < 6; i++)
		{
			var temp = Instantiate (landPrefab) as GameObject;
			temp.transform.parent = gameObject.transform;
			int rXTwo = Random.Range (-2, 2);
			int rYTwo = Random.Range (-2, 2);
			temp.transform.localPosition = new Vector3 (rXTwo, rYTwo, 0);

			landSections.Add(temp);
			//Debug.Log(landSections.Count);
		}
	}

	void CreateContinentInformation()
	{
		// generate information about this continennt
		// population between ten thousand and one billion
		population = Random.Range(10000, 10000000);
		
		// TODO: get area from size of continent
		area = 1000;
		
		// set resources
		resourceOne = ((resources)Random.Range (0, 10)).ToString();
		resourceTwo = ((resources)Random.Range (0, 10)).ToString();
		resourceThree = ((resources)Random.Range (0, 10)).ToString();
		
		// set wealth level randomly
		wealthiness = ((wealth)Random.Range(0, 4)).ToString();
		
		// set laanguage randomly
		language = ((languages)Random.Range(0, 4)).ToString();
		
		// set name randomly
		name = ((names)Random.Range (0, 18)).ToString ();
		
		percentControlled = 0.0f;
		
		Debug.Log (population);
		Debug.Log (area);
		Debug.Log (resourceOne);
		Debug.Log (resourceTwo);
		Debug.Log (resourceThree);
		Debug.Log (wealthiness);
		Debug.Log (language);
		Debug.Log (percentControlled);
	}
	
	public void PurchaseFactory(int FactoryPrice, int MaintenanceCost, int ProductionProfit, string PrimaryResource)
	{
		if (factories.Count < 3)
		{
			// buy a new factory and add it to the list
			var newFactory = Instantiate(factoryPrefab).GetComponent<Factory>();
			newFactory.GenerateFactoryData(population, MaintenanceCost, ProductionProfit, PrimaryResource, wealthiness);
	
			Company.companyMoneyIN += newFactory.productionProfit;
			Company.companyMoneyOUT += newFactory.maintenanceCost;
			
			var landIndex = Random.Range (0, landSections.Count);
			var land = landSections[landIndex];
			var landBounds = land.GetComponent<BoxCollider2D>().size;
			
			newFactory.transform.SetParent(land.transform);
			newFactory.transform.localPosition = new Vector3(Random.Range (-landBounds.x*0.3f,
																		   landBounds.x*0.3f),
			                                                 Random.Range (-landBounds.y*0.3f,
			              												   landBounds.y*0.3f));
			newFactory.transform.localScale = new Vector3(0.8f,0.8f,0.8f);
			
			factories.Add (newFactory);
			Debug.Log (factories.Count);
			Company.companyMoney -= FactoryPrice;
		}
	}
	
	public void DeleteFactory(Factory _factory)
	{
		int count = 0;
		foreach(var f in factories)
		{
			if (f == _factory)
			{
				Destroy(f.gameObject);
				factories.RemoveAt(count);	
				
				Company.companyMoneyIN -= f.productionProfit;
				Company.companyMoneyOUT -= f.maintenanceCost;	
				return;	
			}
			
			count++;
		}
		Debug.LogError("Factory does not exist in this continent");
	}
	
	
	// call when purchasing a new factory
	public float CalculateFactoryPrice(int companyBaseFactoryPrice, string primaryResource)
	{
		// companyBaseFactoryPrice: how much our company is generally charged for a factory
		// wealth: the wealth of the continent we are building this factory on
		// primaryResource: what this factory produces or refines
		// contPop: the population of this conntinent which will affect profits
		
		// this function contains an algorithm to work out how much the user should pay for a factory
		// taking into account all of the above parameters
		
		
		float wealthModifier;
		if(wealthiness == "Impoverished")
			wealthModifier = 0.8f;
		
		else if(wealthiness == "Poor")
			wealthModifier = 0.9f;
		
		else if(wealthiness == "Average")
			wealthModifier = 1.0f;
		
		else if(wealthiness == "Wealthy")
			wealthModifier = 1.2f;
		
		else
			wealthModifier = 1.0f;
		
		float resourceModifier;
		
		switch(primaryResource)
		{
			case "Oil": resourceModifier = 1.8f; break;
			case "Coal": resourceModifier = 1.6f; break;
			case "Diamond": resourceModifier = 2.0f; break;
			case "Copper": resourceModifier = 1.05f; break;
			case "Iron": resourceModifier = 1.5f; break;
			case "Coffee": resourceModifier = 1.4f; break;
			case "Solar": resourceModifier = 1.15f; break;
			case "Tea": resourceModifier = 1.2f; break;
			case "Electronics": resourceModifier = 1.6f; break;
			case "Spent": resourceModifier = 1.0f; break;
			default: resourceModifier = 1.0f; break;
		}
		
		float populationModifier;
		
		if(population < 1000000)
			populationModifier = 1.1f;
		
		else if(population < 5500000)
			populationModifier = 1.05f;
		
		else
			populationModifier = 0.95f;
		
		float finalFactoryPrice = companyBaseFactoryPrice * wealthModifier * resourceModifier * populationModifier;
		
		return finalFactoryPrice;
		
	}
	
	public float calculateFactoryMaintenanceCost(float price)
	{
		//TODO: modify later
		return price*0.2f;
	}
	
	public float calculateFactoryProductionProfit(float price)
	{
		//TODO: modify later
		return price*0.201f;
	}
	
}
