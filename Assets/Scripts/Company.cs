﻿using UnityEngine;
using System.Collections;

public class Company : MonoBehaviour {

	public string companyName;

	public static int companyMoney;
	public static int companyMoneyIN;
	public static int companyMoneyOUT;

	public static int companyBaseFactoryPurchasePrice;
	int basePurchasePrice;
	public static float companyShareholderOpinion;

	float interest;

	int companyAge;

	// Use this for initialization-
	void Start () {
		
		//TODO: Fetch company name from whatever the user inputted on the main menu
		//companyName = MenuScript.Name
		companyName = "Company Name";

		companyMoney = 10000000;
		companyMoneyIN = 0;
		companyMoneyOUT = 0;

		basePurchasePrice = 1000000;
		companyBaseFactoryPurchasePrice = basePurchasePrice;
		companyShareholderOpinion = 80.0f;

		companyAge = 0;
	}
	
	// Update is called once per frame
	void Update () {
		
		// calculate at base how much our company will be charged to buy a factory
		float tempPurPrice = basePurchasePrice/(Company.companyShareholderOpinion / 200.0f);
		companyBaseFactoryPurchasePrice = (int)tempPurPrice;

		companyAge = TimeManager.TIME_ELAPSED_IN_MONTHS;

		if (companyShareholderOpinion > 100.0f)
			companyShareholderOpinion = 100.0f;
			
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			Application.Quit ();
		}

		//Debug.Log (companyMoneyIN);
		//Debug.Log (companyMoneyOUT);

		//Debug.Log (companyAge);
		//Debug.Log (companyMoney);
	}
}