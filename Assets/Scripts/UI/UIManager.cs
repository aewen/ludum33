﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

	Canvas canvas;
	private UIGlobalStats globalStats;
	private UIMoneyStats moneyStats;
	private UIContinentStats continentStats;
	private UIPurchaseFactory purchaseStats;
	private UIFactoryStats factoryStats;
	private UIFactorySelect factorySelect;
	
	private GameObject world;
	
	//REPLACE THESE WITH ACTUAL STATS FROM PLAYER
	private int totalDollars;
	private float shareholderOpinion;
	
	private bool selectedFlag = false;
	private bool trueFlag = false;
	
	// Use this for initialization
	void Start () {
		
		totalDollars = 0;
		shareholderOpinion = Company.companyShareholderOpinion;
	 
		canvas = GetComponentInChildren<Canvas>(); 
		canvas.enabled = true;
		
		globalStats = GetComponentInChildren<UIGlobalStats>();
		moneyStats = GetComponentInChildren<UIMoneyStats> ();
		continentStats = GetComponentInChildren<UIContinentStats>();
		purchaseStats = GetComponentInChildren<UIPurchaseFactory>();
		factoryStats = GetComponentInChildren<UIFactoryStats>();
		factorySelect = GetComponentInChildren<UIFactorySelect>();
		
		world = GameObject.Find("World");
	}
	
	// Update is called once per frame
	void Update () {
		totalDollars = Company.companyMoney;
		
		trueFlag = false;
		
		foreach (Continent continent in world.GetComponent<World>().continents)
		{
			if (continent.GetSelected() && !selectedFlag)
			{
				continentStats.SetContinent(continent);
				purchaseStats.SetContinent(continent);
				factoryStats.SetContinent(continent);
				factorySelect.SetContinent(continent);
				selectedFlag = true;
			}
			
			if (continent.GetSelected())
			{
				trueFlag = true;
			}
		}
		
		if (!trueFlag && selectedFlag)
		{
			Debug.Log("DESELECTING CONTINENT");
			selectedFlag = false;
			continentStats.SetContinent(null);
			purchaseStats.SetContinent(null);
			factoryStats.SetContinent(null);
			factorySelect.SetContinent(null);
		}
		
		globalStats.SetStats(totalDollars, (int)shareholderOpinion);
		moneyStats.SetStats (Company.companyMoneyIN, Company.companyMoneyOUT);
		
	}
}
