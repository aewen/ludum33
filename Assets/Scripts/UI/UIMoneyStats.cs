﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIMoneyStats : UIPanel {

	private RectTransform panel;
	
	private int moneyInTotal;
	private int moneyOutTotal;
	
	private GameObject moneyStatsText;
	private string moneyStatsValue;
	
	// Use this for initialization
	public override void InitializePanel () {
		
		moneyInTotal = 0;
		moneyOutTotal = 0;
		
		panel = GetComponent<RectTransform>();
		
		//left, bottom
		//panel.offsetMin = new Vector2(0.0f, canvasHeight*0.9f);
		
		//right, top
		//panel.offsetMax = new Vector2(-canvasWidth/2, 0.0f);
		
		moneyStatsText = GameObject.Find("TextMoneyStats");
		//globalStatsText.GetComponent<RectTransform>().offsetMin = new Vector2(, globalStatsText.GetComponent<RectTransform>().offsetMin.y)
		
		
		UpdateText();
		
		
		moneyStatsText.GetComponent<Text>().text = moneyStatsValue;
		
	}
	
	// Update is called once per frame
	public override void UpdatePanel () {
		//left, bottom
		panel.offsetMin = new Vector2(0.0f, canvasHeight*0.8f);
		
		//right, top
		panel.offsetMax = new Vector2(-canvasWidth/1.4f, -canvasHeight*0.1f);
		
		UpdateText();
	}
	
	public void SetStats (int _currentMoneyInValue, int _currentMoneyOutValue) 
	{
		moneyInTotal = _currentMoneyInValue;
		moneyOutTotal = _currentMoneyOutValue;
	}
	
	private void UpdateText()
	{  
		if(moneyInTotal - moneyOutTotal < 0)
			moneyStatsValue = "Net Income: <color=#FF0000FF>$" + (moneyInTotal - moneyOutTotal).ToString("n0") + "</color>";
		else
			moneyStatsValue = "Net Income: <color=#00FF00FF>$" + (moneyInTotal - moneyOutTotal).ToString("n0") + "</color>";
		
		moneyStatsText.GetComponent<Text>().text = moneyStatsValue;	
	}
}
