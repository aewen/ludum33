﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIEventWindow : UIPanel {
	
	private RectTransform panel;
	private GameObject eventText;
	private GameObject buttonConfirmEvent;
	private string textDesc;
	
	public override void InitializePanel ()
	{
		panel = GetComponent<RectTransform>();
		eventText = GameObject.Find("TextEventDesc");
		buttonConfirmEvent = GameObject.Find("ButtonConfirmEvent");
		
		Color tempColor = GetComponent<Image>().color;
		tempColor.a = 1.0f;
		GetComponent<Image>().color = tempColor;
		
		GetComponent<Image>().enabled = false;
		GetComponentInChildren<Text>().enabled = false;
		buttonConfirmEvent.GetComponent<Image>().enabled = false;
		buttonConfirmEvent.GetComponentInChildren<Text>().enabled = false;
		
		//left, bottom
		panel.offsetMin = new Vector2(canvasWidth*0.3f, canvasHeight*0.2f);
		
		//right, top
		panel.offsetMax = new Vector2(-canvasWidth*0.3f, -canvasHeight*0.2f);		
		
	}
	
	public override void UpdatePanel ()
	{
		if (textDesc != null)
		{
			eventText.GetComponent<Text>().text = textDesc;	
		}
	}
	
	public void DisplayPanel()
	{
		Debug.Log("displaying event window");
		GetComponent<Image>().enabled = true;
		GetComponentInChildren<Text>().enabled = true;
		buttonConfirmEvent.GetComponent<Image>().enabled = true;
		buttonConfirmEvent.GetComponentInChildren<Text>().enabled = true;
	}
	
	public void HideWindow()
	{
		GetComponent<Image>().enabled = false;
		GetComponentInChildren<Text>().enabled = false;	
		buttonConfirmEvent.GetComponent<Image>().enabled = false;
		buttonConfirmEvent.GetComponentInChildren<Text>().enabled = false;	
	}
	
	public void SetText(string _text)
	{
		textDesc = _text;
	}
	
}
