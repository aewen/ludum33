﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIGlobalStats : UIPanel {

	private RectTransform panel;
	
	private int dollarTotal;
	private int shareholderOpinion;
	
	private GameObject globalStatsText;
	private string globalStatsValue;

	// Use this for initialization
	public override void InitializePanel () {

		dollarTotal = 0;
		shareholderOpinion = 100;
	
		panel = GetComponent<RectTransform>();
		
		//left, bottom
		//panel.offsetMin = new Vector2(0.0f, canvasHeight*0.9f);
		
		//right, top
		//panel.offsetMax = new Vector2(-canvasWidth/2, 0.0f);
		
		globalStatsText = GameObject.Find("TextGlobalStats");
		//globalStatsText.GetComponent<RectTransform>().offsetMin = new Vector2(, globalStatsText.GetComponent<RectTransform>().offsetMin.y)
		
		
		UpdateText();
		
		
	 	globalStatsText.GetComponent<Text>().text = globalStatsValue;
		
	}
	
	// Update is called once per frame
	public override void UpdatePanel () {
		//left, bottom
		panel.offsetMin = new Vector2(0.0f, canvasHeight*0.9f);
		
		//right, top
		panel.offsetMax = new Vector2(-canvasWidth/4, 0.0f);
		
		UpdateText();
	}
	
	public void SetStats (int _currentDollarValue, int _currentshareholderOpinion) 
	{
		dollarTotal = _currentDollarValue;
		shareholderOpinion = _currentshareholderOpinion;
	}
	
	private void UpdateText()
	{  
		globalStatsValue = "$" + dollarTotal.ToString("n0") + "        Shareholder Opinion: " + Company.companyShareholderOpinion + "%"
			+ "        Year: " + TimeManager.TIME_ELAPSED_IN_YEARS + "   Month: " + (TimeManager.TIME_ELAPSED_IN_MONTHS % 12);
			
		globalStatsText.GetComponent<Text>().text = globalStatsValue;	
	}
}
