﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIContinentStats : UIPanel {
	
	private RectTransform panelContinentStats;
	
	private GameObject panelBackButton;
	private GameObject panelPurchaseButton;
	private GameObject panelPurchaseFactory;
	private GameObject panelFactoryStatsButton;
	private GameObject panelFactorySelect;
	
	private string ContinentStatsString;
	
	private GameObject ContinentStatsText;
	
	private Continent selectedContinent;
	
	public override void InitializePanel()
	{
		ContinentStatsText = GameObject.Find("TextContinentStats");
		panelBackButton = GameObject.Find("ButtonBack");
		panelPurchaseButton = GameObject.Find("ButtonPurchaseFactory");
		panelPurchaseFactory = GameObject.Find("PurchaseFactoryStats");
		panelFactoryStatsButton = GameObject.Find("ButtonFactoryStats");
		panelFactorySelect = GameObject.Find("FactorySelect");
		
		GetComponent<Image>().enabled = false;
		GetComponentInChildren<Text>().enabled = false;
		
		panelBackButton.GetComponent<Image>().enabled = false;
		panelBackButton.GetComponentInChildren<Text>().enabled = false;
				
		panelPurchaseButton.GetComponent<Image>().enabled = false;
		panelPurchaseButton.GetComponentInChildren<Text>().enabled = false;
		
		panelFactoryStatsButton.GetComponent<Image>().enabled = false;
		panelFactoryStatsButton.GetComponentInChildren<Text>().enabled = false;
		
		panelContinentStats = GetComponent<RectTransform>();
		
		//left, bottom
		panelContinentStats.offsetMin = new Vector2(canvasWidth*0.5f, canvasHeight*0.05f);
		
		//right, top
		panelContinentStats.offsetMax = new Vector2(-canvasWidth*0.05f, -canvasHeight*0.1f);
		
	}
	
	public override void UpdatePanel()
	{
		if (selectedContinent != null)
		{
			string factoriesString = "";
			int count = 0;
			foreach (Factory factory in selectedContinent.factories)
			{
				factoriesString += factory.factoryResource + "\tFactory " + "\n";
				count++;
			}
			
			
			ContinentStatsString = 	"<b><size=24>" + selectedContinent.name + "</size></b>\n" + "<b>Population: </b>" + selectedContinent.population.ToString("n0") + 
			"\n\n<b>Resource 1: </b>" + selectedContinent.resourceOne + "\n<b>Resource 2: </b>" + selectedContinent.resourceTwo + 
			"\n<b>Resource 3: </b>" + selectedContinent.resourceThree + "\n\n<b>Wealthiness: </b>" + selectedContinent.wealthiness + 
			"\n\n<b>Language: </b>" + selectedContinent.language + "\n<b>Factories: </b>\n" + factoriesString;	
			GetComponent<Image>().enabled = true;
			GetComponentInChildren<Text>().enabled = true;	
			
			panelBackButton.GetComponent<Image>().enabled = true;
			panelBackButton.GetComponentInChildren<Text>().enabled = true;	
			
			panelPurchaseButton.GetComponent<Image>().enabled = true;
			panelPurchaseButton.GetComponentInChildren<Text>().enabled = true;	
			
			panelFactoryStatsButton.GetComponent<Image>().enabled = true;
			panelFactoryStatsButton.GetComponentInChildren<Text>().enabled = true;					
		}
		else
		{
			GetComponent<Image>().enabled = false;
			GetComponentInChildren<Text>().enabled = false;	
			
			panelBackButton.GetComponent<Image>().enabled = false;
			panelBackButton.GetComponentInChildren<Text>().enabled = false;	
			
			panelPurchaseButton.GetComponent<Image>().enabled = false;
			panelPurchaseButton.GetComponentInChildren<Text>().enabled = false;

			panelFactoryStatsButton.GetComponent<Image>().enabled = false;
			panelFactoryStatsButton.GetComponentInChildren<Text>().enabled = false;			
		}
		
		ContinentStatsText.GetComponent<Text>().text = ContinentStatsString;
	}
	
	public void SetContinent(Continent _selectedContinent)
	{
		selectedContinent = _selectedContinent;
		panelPurchaseFactory.GetComponent<UIPurchaseFactory>().SetContinent(selectedContinent);
		panelFactorySelect.GetComponent<UIFactorySelect>().SetContinent(selectedContinent);
	}

	
	public void OnBack()
	{
		if (selectedContinent != null)
			selectedContinent.SetSelected(false);
	}
	
	
	
}
