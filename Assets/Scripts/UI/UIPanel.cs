﻿using UnityEngine;
using System.Collections;

public abstract class UIPanel : MonoBehaviour {

	protected float canvasWidth, canvasHeight;
	protected GameObject canvas;
	
	// Use this for initialization
	void Start () {
		InitializeCanvas();
		InitializePanel();
	}
	
	// Update is called once per frame
	void Update () {
		UpdateDimensions();
		UpdatePanel();
	}
	
	protected void InitializeCanvas()
	{
		canvas = GameObject.Find("Canvas");
		canvasWidth = canvas.GetComponent<RectTransform>().rect.width;
		canvasHeight = canvas.GetComponent<RectTransform>().rect.height;	
	}
	
	protected void UpdateDimensions()
	{
		canvasWidth = canvas.GetComponent<RectTransform>().rect.width;
		canvasHeight = canvas.GetComponent<RectTransform>().rect.height;	
	}
	
	abstract public void InitializePanel();
	
	abstract public void UpdatePanel();
}
