﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIPurchaseFactory : UIPanel {
	private RectTransform panelPurchaseFactory;
	private GameObject buttonConfirmPurchase, buttonConfirmResource1, buttonConfirmResource2, buttonConfirmResource3;
	private string primaryResource, TextPurchaseFactory;
	
	private Continent selectedContinent;
	
	private string resource1, resource2, resource3;
	
	private float basePrice, maintenanceCost, productionProfit;
	
	public override void InitializePanel () 
	{
		basePrice = 0;
	
		buttonConfirmPurchase = GameObject.Find("ButtonConfirmPurchase");
		buttonConfirmResource1 = GameObject.Find("ButtonConfirmResource1");
		buttonConfirmResource2 = GameObject.Find("ButtonConfirmResource2");
		buttonConfirmResource3 = GameObject.Find("ButtonConfirmResource3");
	
		GetComponent<Image>().enabled = false;
		GetComponentInChildren<Text>().enabled = false;
		
		buttonConfirmPurchase.GetComponent<Image>().enabled = false;
		buttonConfirmPurchase.GetComponentInChildren<Text>().enabled = false;
		
		
		buttonConfirmResource1.GetComponent<Image>().enabled = false;
		buttonConfirmResource1.GetComponentInChildren<Text>().enabled = false;
		buttonConfirmResource2.GetComponent<Image>().enabled = false;
		buttonConfirmResource2.GetComponentInChildren<Text>().enabled = false;
		buttonConfirmResource3.GetComponent<Image>().enabled = false;
		buttonConfirmResource3.GetComponentInChildren<Text>().enabled = false;
		

		
		panelPurchaseFactory = gameObject.GetComponent<RectTransform>();
		
		panelPurchaseFactory.offsetMin = new Vector2(canvasWidth*0.05f, canvasHeight*0.05f);
		panelPurchaseFactory.offsetMax = new Vector2(-canvasWidth*0.5f, -canvasHeight*0.3f);
		
	}
	
	public override void UpdatePanel()
	{
		if (selectedContinent != null)
		{
			resource1 = selectedContinent.resourceOne;
			resource2 = selectedContinent.resourceTwo;
			resource3 = selectedContinent.resourceThree;	
			
			buttonConfirmResource1.GetComponentInChildren<Text>().text = resource1;		
			buttonConfirmResource2.GetComponentInChildren<Text>().text = resource2;
			buttonConfirmResource3.GetComponentInChildren<Text>().text = resource3;	
			
			if (primaryResource == null)
				primaryResource = resource1;
					
			basePrice = selectedContinent.CalculateFactoryPrice(Company.companyBaseFactoryPurchasePrice, primaryResource);
			maintenanceCost = selectedContinent.calculateFactoryMaintenanceCost(basePrice);
			productionProfit = selectedContinent.calculateFactoryProductionProfit(basePrice);
			
								
			TextPurchaseFactory = "<b>Primary Resource: </b>" + primaryResource + "\nBase Price: $" + basePrice.ToString("n0")
				+ "\n<b>Maintenance Cost: </b>$" + maintenanceCost.ToString("n0") + "\n<b>Production Profit: </b>$" + productionProfit.ToString("n0");
			GetComponentInChildren<Text>().text = TextPurchaseFactory;			
		}
		else
		{
			primaryResource = null;	
		}
	}
	
	public void displayPanel()
	{
		GetComponent<Image>().enabled = true;
		GetComponentInChildren<Text>().enabled = true;	
		buttonConfirmPurchase.GetComponent<Image>().enabled = true;	
		buttonConfirmPurchase.GetComponentInChildren<Text>().enabled = true;
		
		buttonConfirmResource1.GetComponent<Image>().enabled = true;
		buttonConfirmResource1.GetComponentInChildren<Text>().enabled = true;
		buttonConfirmResource2.GetComponent<Image>().enabled = true;
		buttonConfirmResource2.GetComponentInChildren<Text>().enabled = true;
		buttonConfirmResource3.GetComponent<Image>().enabled = true;
		buttonConfirmResource3.GetComponentInChildren<Text>().enabled = true;		
	}
	
	public void hidePanel()
	{
		GetComponent<Image>().enabled = false;
		GetComponentInChildren<Text>().enabled = false;
		buttonConfirmPurchase.GetComponent<Image>().enabled = false;
		buttonConfirmPurchase.GetComponentInChildren<Text>().enabled = false;
		
		buttonConfirmResource1.GetComponent<Image>().enabled = false;
		buttonConfirmResource1.GetComponentInChildren<Text>().enabled = false;
		buttonConfirmResource2.GetComponent<Image>().enabled = false;
		buttonConfirmResource2.GetComponentInChildren<Text>().enabled = false;
		buttonConfirmResource3.GetComponent<Image>().enabled = false;
		buttonConfirmResource3.GetComponentInChildren<Text>().enabled = false;
	}
	
	
	public void SetContinent(Continent _selectedContinent)
	{
		selectedContinent = _selectedContinent;
		
		if (selectedContinent != null)
		{
			resource1 = selectedContinent.resourceOne;
			resource2 = selectedContinent.resourceTwo;
			resource3 = selectedContinent.resourceThree;	
			
			buttonConfirmResource1.GetComponentInChildren<Text>().text = resource1;		
			buttonConfirmResource2.GetComponentInChildren<Text>().text = resource2;
			buttonConfirmResource3.GetComponentInChildren<Text>().text = resource3;				
		}
		
		
	}
	
	public void SetPrimaryResource(int _resource)
	{
		string currentResource = " ";

		if (_resource == 0)
			currentResource = resource1;
		else if (_resource == 1)
			currentResource = resource2;
		else if (_resource == 2)
			currentResource = resource3;
				
		primaryResource = currentResource;
	}
	
	public void OnPurchase()
	{
		selectedContinent.PurchaseFactory((int)basePrice, (int)maintenanceCost, (int)productionProfit, primaryResource);
	}
}
