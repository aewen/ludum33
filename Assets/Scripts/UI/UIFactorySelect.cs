﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;


public class UIFactorySelect : UIPanel {

	private RectTransform panel;
	private Continent selectedContinent;
	private GameObject buttonSelect1, buttonSelect2, buttonSelect3;
	public List<GameObject> buttonSelectList = new List<GameObject>();

	public override void InitializePanel ()
	{
		buttonSelectList.Add(GameObject.Find("ButtonSelectFactory1"));
		buttonSelectList.Add(GameObject.Find("ButtonSelectFactory2"));
		buttonSelectList.Add(GameObject.Find("ButtonSelectFactory3"));
	
		panel = GetComponent<RectTransform>();
		
		GetComponent<Image>().enabled = false;
		//GetComponentInChildren<Text>().enabled = false;
		
						
		foreach(GameObject button in buttonSelectList)
		{
			button.GetComponent<Image>().enabled = false;
			button.GetComponentInChildren<Text>().enabled = false;
		}
		

		panel.offsetMin = new Vector2(canvasWidth*0.05f, canvasHeight*0.05f);
		panel.offsetMax = new Vector2(-canvasWidth*0.5f, -canvasHeight*0.3f);
		
				
	}
	
	public override void UpdatePanel ()
	{
		if (selectedContinent != null)
		{
			int count = 0;
			foreach(Factory factory in selectedContinent.factories)
			{
				if (count <= buttonSelectList.Count)		
				{
					buttonSelectList[count].GetComponentInChildren<Text>().text = factory.factoryResource + " Factory";				
				}
				
				count++;
			}
		}
		else
		{
			
		}
	}
	
	public void DisplayPanel()
	{
		GetComponent<Image>().enabled = true;
		//GetComponentInChildren<Text>().enabled = true;	
		foreach(GameObject button in buttonSelectList)
		{
			button.GetComponent<Image>().enabled = true;
			button.GetComponentInChildren<Text>().enabled = true;
		}	
	}
	
	public void HidePanel()
	{
		if (selectedContinent.factories.Count > 0)
		{
			GetComponent<Image>().enabled = false;
			//GetComponentInChildren<Text>().enabled = false;
			foreach(GameObject button in buttonSelectList)
			{
				button.GetComponent<Image>().enabled = false;
				button.GetComponentInChildren<Text>().enabled = false;
			}			
		}
	}
	
	public void HidePanelNoCheck()
	{
		GetComponent<Image>().enabled = false;
		//GetComponentInChildren<Text>().enabled = false;
		foreach(GameObject button in buttonSelectList)
		{
			button.GetComponent<Image>().enabled = false;
			button.GetComponentInChildren<Text>().enabled = false;
		}			
	}
	
	public void SetContinent(Continent _selectedContinent)
	{
		selectedContinent = _selectedContinent;
		for (int i = 0; i < buttonSelectList.Count; i++)
		{
			buttonSelectList[i].GetComponentInChildren<Text>().text = "No factory";
		}
	}
}
