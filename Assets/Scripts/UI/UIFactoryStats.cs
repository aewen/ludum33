﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIFactoryStats : UIPanel {
	private RectTransform panel;
	private Continent selectedContinent;
	private int factoryNumber;
	private GameObject buttonIncreaseWages, buttonDecreaseWages, buttonIncreaseHours, buttonDecreaseHours;
	private string stringFactoryStats;
	private GameObject FactoryStatsText;
	
	
	public override void InitializePanel ()
	{
		factoryNumber = -1;
		GetComponent<Image>().enabled = false;
		
		FactoryStatsText = GameObject.Find("TextFactoryStats");
		buttonIncreaseWages = GameObject.Find("ButtonIncreaseWages");
		buttonDecreaseWages = GameObject.Find("ButtonDecreaseWages");
		buttonIncreaseHours = GameObject.Find("ButtonIncreaseHours");
		buttonDecreaseHours = GameObject.Find("ButtonDecreaseHours");
		
		
		buttonIncreaseWages.GetComponent<Image>().enabled = false;
		buttonIncreaseWages.GetComponentInChildren<Text>().enabled = false;
		buttonDecreaseWages.GetComponent<Image>().enabled = false;
		buttonDecreaseWages.GetComponentInChildren<Text>().enabled = false;
		buttonIncreaseHours.GetComponent<Image>().enabled = false;
		buttonIncreaseHours.GetComponentInChildren<Text>().enabled = false;
		buttonDecreaseHours.GetComponent<Image>().enabled = false;
		buttonDecreaseHours.GetComponentInChildren<Text>().enabled = false;
		
		
		panel = gameObject.GetComponent<RectTransform>();
		 
		panel.offsetMin = new Vector2(canvasWidth*0.05f, canvasHeight*0.05f);
		panel.offsetMax = new Vector2(-canvasWidth*0.5f, -canvasHeight*0.3f);
	}
	
	public override void UpdatePanel ()
	{
		if (selectedContinent != null && factoryNumber != -1 && selectedContinent.factories.Count > 0)
		{
			stringFactoryStats = "<b>" + selectedContinent.factories[factoryNumber].factoryResource + " Factory</b>\n" +
				"Factory Wages: $" + (selectedContinent.factories[factoryNumber].wagePerMonth / 10.0f).ToString("n0") + "\nFactory Hours: " + 
					selectedContinent.factories[factoryNumber].workHours + "\nFactory Maintenance Cost: $" + selectedContinent.factories[factoryNumber].maintenanceCost.ToString("n0")
					+ "\nFactory Income: $" + selectedContinent.factories[factoryNumber].productionProfit.ToString("n0")
					+ "\n\nFactory Happiness: ";

			switch(selectedContinent.factories[factoryNumber].opinion)
			{
			case -2: 	stringFactoryStats += "<color=#CC0000FF>Terrible</color>"; break;
			case -1:	stringFactoryStats += "<color=#663333FF>Poor</color>"; break;
			case 0:		stringFactoryStats += "<color=#333333FF>Average</color>"; break;
			case 1:		stringFactoryStats += "<color=#336633FF>Good</color>"; break;
			case 2:		stringFactoryStats += "<color=#00CC00FF>Great</color>"; break;
			default:	stringFactoryStats += "NULL"; break;
			}
					
			
		}
		FactoryStatsText.GetComponent<Text>().text = stringFactoryStats;	
	}
	
	public void DisplayPanel()
	{
		if (selectedContinent.factories.Count > 0)
		{
		
			
			GetComponent<Image>().enabled = true;
			//GetComponentInChildren<Text>().enabled = false;	
			
			buttonIncreaseWages.GetComponent<Image>().enabled = true;
			buttonIncreaseWages.GetComponentInChildren<Text>().enabled = true;
			buttonDecreaseWages.GetComponent<Image>().enabled = true;
			buttonDecreaseWages.GetComponentInChildren<Text>().enabled = true;
			buttonIncreaseHours.GetComponent<Image>().enabled = true;
			buttonIncreaseHours.GetComponentInChildren<Text>().enabled = true;
			buttonDecreaseHours.GetComponent<Image>().enabled = true;
			buttonDecreaseHours.GetComponentInChildren<Text>().enabled = true;	
			
			FactoryStatsText.GetComponent<Text>().enabled = true;			
		}

	}
	
	public void HidePanel()
	{
		GetComponent<Image>().enabled = false;
		//GetComponentInChildren<Text>().enabled = false;	
		
		buttonIncreaseWages.GetComponent<Image>().enabled = false;
		buttonIncreaseWages.GetComponentInChildren<Text>().enabled = false;
		buttonDecreaseWages.GetComponent<Image>().enabled = false;
		buttonDecreaseWages.GetComponentInChildren<Text>().enabled = false;
		buttonIncreaseHours.GetComponent<Image>().enabled = false;
		buttonIncreaseHours.GetComponentInChildren<Text>().enabled = false;
		buttonDecreaseHours.GetComponent<Image>().enabled = false;
		buttonDecreaseHours.GetComponentInChildren<Text>().enabled = false;	
		
		FactoryStatsText.GetComponent<Text>().enabled = false;
	}
	
	public void SetContinent (Continent _selectedContinent)
	{
		selectedContinent = _selectedContinent;
	}
	
	public void SetFactoryNumber (int _factoryNumber)
	{
		factoryNumber = _factoryNumber;
	}
	
	public void IncreaseWages()
	{
		selectedContinent.factories[factoryNumber].AdjustFactorySettings(0, 500);
	}
	
	public void DecreaseWages()
	{
		selectedContinent.factories[factoryNumber].AdjustFactorySettings(0, -500);
	}
	
	public void IncreaseHours()
	{
		selectedContinent.factories[factoryNumber].AdjustFactorySettings(1, 0);
	}
	
	public void DecreaseHours()
	{
		selectedContinent.factories[factoryNumber].AdjustFactorySettings(-1, 0);
	}
	
	
}
