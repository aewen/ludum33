﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIEventWindowDecision : UIPanel {

	private RectTransform panel;
	private string textDesc; 
	private GameObject eventDecisionText;
	private GameObject buttonOption1, buttonOption2;
	
	private Event.GameEvent firedEvent;

	public override void InitializePanel ()
	{
		eventDecisionText = GameObject.Find("TextEventDecisionDesc");
		buttonOption1 = GameObject.Find("ButtonOption1");
		buttonOption2 = GameObject.Find("ButtonOption2");
	
		Color tempColor = GetComponent<Image>().color;
		tempColor.a = 1.0f;
		GetComponent<Image>().color = tempColor;
	
		panel = GetComponent<RectTransform>();
		
		GetComponent<Image>().enabled = false;
		GetComponentInChildren<Text>().enabled = false;	
		GetComponent<Image>().enabled = false;
		GetComponentInChildren<Text>().enabled = false;	
		buttonOption1.GetComponent<Image>().enabled = false;
		buttonOption1.GetComponentInChildren<Text>().enabled = false;	
		buttonOption2.GetComponent<Image>().enabled = false;
		buttonOption2.GetComponentInChildren<Text>().enabled = false;
		
		
				
		//left, bottom
		panel.offsetMin = new Vector2(canvasWidth*0.3f, canvasHeight*0.2f);
		
		//right, top
		panel.offsetMax = new Vector2(-canvasWidth*0.3f, -canvasHeight*0.2f);		
		
			
				
	}
	
	public override void UpdatePanel ()
	{
		if (textDesc != null)
		{
			eventDecisionText.GetComponent<Text>().text = textDesc;	
		}		
	}
	
	
	public void DisplayPanel()
	{
		Debug.Log("displaying event window");
		GetComponent<Image>().enabled = true;
		GetComponentInChildren<Text>().enabled = true;
		buttonOption1.GetComponent<Image>().enabled = true;
		buttonOption1.GetComponentInChildren<Text>().enabled = true;
		buttonOption2.GetComponent<Image>().enabled = true;
		buttonOption2.GetComponentInChildren<Text>().enabled = true;
	}
	
	public void HideWindow()
	{
		GetComponent<Image>().enabled = false;
		GetComponentInChildren<Text>().enabled = false;	
		buttonOption1.GetComponent<Image>().enabled = false;
		buttonOption1.GetComponentInChildren<Text>().enabled = false;	
		buttonOption2.GetComponent<Image>().enabled = false;
		buttonOption2.GetComponentInChildren<Text>().enabled = false;		

	}
	
	public void SetText(string _text)
	{
		textDesc = _text;
	}
	
	public void SetFiredEvent(Event.GameEvent _firedEvent)
	{
		firedEvent = _firedEvent;
	}
	
	
	public void ExecuteOption1()
	{
		if (firedEvent != null)
		{
			firedEvent.executeOptionA();
		}
	}
	
	public void ExecuteOption2()
	{
		if (firedEvent != null)
		{
			firedEvent.executeOptionB();
		}
	}	
}
