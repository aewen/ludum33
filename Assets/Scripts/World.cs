﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class World : MonoBehaviour {

	public Continent continentPrefab;
	public List<Continent> continents = new List<Continent>();
	static int continentCount = 6;

	// Use this for initialization
	void Start () {
	
		for (int j = 0; j < continentCount; j++)
		{
			Continent continent = Instantiate(continentPrefab) as Continent;
			continent.transform.parent = gameObject.transform;
			int rXOne = Random.Range (-15, 15);
			int rYOne = Random.Range (-5, 5);
			continent.transform.position = new Vector3(rXOne, rYOne, 0);

			continent.RedrawLand();
		
			for(int i = 0; i < j; i++)
			{
				for(int k = 0; k < continent.landSections.Count; k++)
				{
					foreach(GameObject tempsLand in continents[i].landSections)
					{
						while(continent.landSections[k].GetComponent<SpriteRenderer>().bounds.Intersects(tempsLand.GetComponent<SpriteRenderer>().bounds))
						{
							Debug.Log("Moved a continent");

							rXOne = Random.Range (-15, 15);
							rYOne = Random.Range (-5, 5);
							continent.transform.position = new Vector3(rXOne, rYOne, 0);

							continent.RedrawLand();

							k = 0;
							i = 0;
						}
					}
				}
			}

			continents.Add (continent);
			Debug.Log (continents.Count);
		}
	}
			
	// Update is called once per frame
	void Update () {
	
		int globalPopulation = 0;
		
		for(int i = 0; i < continentCount; i++)
		{
			globalPopulation+=continents[i].GetComponent<Continent>().population;
		}
		
		//Debug.Log ("Total global population " + globalPopulation);
		
		CheckMouse ();

	}

	void CheckMouse() {
		
		if(!anyContinentSelected())
		{
			RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
			
			if(hit.collider != null)
			{
				bool allowSelect = true;
				foreach (Continent continent in continents)
				{
					if(continent.SelectInterp > 0.0f) allowSelect = false;
					foreach (GameObject land in continent.landSections)
					{
						if(hit.collider.gameObject == land.gameObject)
						{
								if (Input.GetMouseButtonUp(0))
								{
									if (allowSelect) continent.SetSelected(true);
								}
								else
								{
									continent.SetHover(true);
								}
								break;
						}
						else
						{
							if (Input.GetMouseButtonUp(0))
								continent.SetSelected(false);
							else
								continent.SetHover(false);
						}
					}
				}
			}
			else
			{
				foreach(var continent in continents)
				{
					continent.SetHover(false);
				}
			}
		}
	}
	
	public bool anyContinentSelected() 
	{
		foreach(var continent in continents)
		{
			if (continent.GetSelected()) return true;
		}
		return false;
	}
}
