﻿using UnityEngine;
using System.Collections;

public class EventManager : MonoBehaviour {

	private GameObject world;

	
	Event eventController = new Event();
	Event.GameEvent.WarBreakout warBreakoutEvent;
	Event.GameEvent.WarOver warOverEvent;
	Event.GameEvent.TestOptionEvent optionEvent;
	Event.GameEvent.MoneyBelowZero moneyBelowZeroEvent;
	Event.GameEvent.WorkerProtestEvent workerProtestEvent;
	Event.GameEvent.InternationalInvestigationEvent internationalInvestigationEvent;
	Event.GameEvent.EnvironmentalDisasterEvent environmentalDisasterEvent;
	Event.GameEvent.BankruptEvent bankruptEvent;
	Event.GameEvent.WorkerSuicideEvent workerSuicideEvent;
	Event.GameEvent.IdleCompanyEvent idleCompanyEvent;
	Event.GameEvent.ShareholdersAbandonEvent shareholdersAbandonEvent;


	
	// if there is a continent at war, this stores what number it is
	int continentAtWar = -1;
	int lastYearIncome = 0;

	int thisYear = 1;

	// remember when you went into the red
	int deadlineMonth = -1;

	// Use this for initialization
	void Start () {
		world = GameObject.Find("World");
		moneyBelowZeroEvent = new Event.GameEvent.MoneyBelowZero();
		warBreakoutEvent = new Event.GameEvent.WarBreakout();
		warOverEvent = new Event.GameEvent.WarOver();
		bankruptEvent = new Event.GameEvent.BankruptEvent();
		shareholdersAbandonEvent = new Event.GameEvent.ShareholdersAbandonEvent();
	}
	
	// Update is called once per frame
	void Update () {
		
		//example of how this should work
		//if(Company.companyMoney < 0)
		//	FireEvent(NoMoneyEvent);
		//
		//if(TimeManager.TimeElapsedInYears > 30)
		// FireEvent(EndOfGameEvent);
		
		if(Company.companyMoney < 0 && !moneyBelowZeroEvent.active)
		{
			eventController.FireEvent (moneyBelowZeroEvent);
			eventController.SetEventActive(moneyBelowZeroEvent, true);
			deadlineMonth = TimeManager.TIME_ELAPSED_IN_MONTHS+12;
		}
		if(moneyBelowZeroEvent.active)
		{
			moneyBelowZeroEvent.ongoingEvent();
		}
		if(Company.companyMoney > 0)
		{
			eventController.SetEventActive(moneyBelowZeroEvent, false);
		}
		
		if((Company.companyMoney < 0) && moneyBelowZeroEvent.active && (deadlineMonth == TimeManager.TIME_ELAPSED_IN_MONTHS))
		{
			eventController.FireOptionalEvent(bankruptEvent);
		}
		
		if(Company.companyShareholderOpinion < 0)
		{
			eventController.FireOptionalEvent(shareholdersAbandonEvent);
		}
		
		// randomly pick a continent to go to war
		// poor countries more likely to go to war
		if(continentAtWar == -1)
		{
			continentAtWar = Random.Range (1, 6);
		}
		
		int chanceToGoToWar = 900000000;
		switch(world.GetComponent<World>().continents[continentAtWar].wealthiness)
		{
			case "Impoverished": chanceToGoToWar = 1000; break;
			case "Poor": chanceToGoToWar 		 = 10000; break;
			case "Average": chanceToGoToWar		 = 100000; break;
			case "Wealthy": chanceToGoToWar		 = 1000000; break;
			default: chanceToGoToWar			 = 100000000; break;
		}
		
		// War Event
		if(Random.Range (1, chanceToGoToWar) < 5 && !warBreakoutEvent.active)
		{
			eventController.FireEvent (warBreakoutEvent, world.GetComponent<World>().continents[continentAtWar]);
			eventController.SetEventActive(warBreakoutEvent, true);
			world.GetComponent<World>().continents[continentAtWar].atWar = true;
			Debug.Log ("War started");
		}
		if(Random.Range (1, 100000) < 5 && warBreakoutEvent.active)
		{
			eventController.FireEvent(warOverEvent, world.GetComponent<World>().continents[continentAtWar]);
			eventController.SetEventActive(warBreakoutEvent, false);
			world.GetComponent<World>().continents[continentAtWar].atWar = false;
			continentAtWar = -1;
			Debug.Log ("War over");
		}
		

		
		foreach(Continent continent in world.GetComponent<World>().continents)
		{
		
			if (continent.factories.Count == 0)
			{
				if (Random.Range(1, 600000) < 5)
				{
					Event.GameEvent.PurchaseNewFactoryEvent purchaseNewFactoryEvent;
					purchaseNewFactoryEvent = new Event.GameEvent.PurchaseNewFactoryEvent(continent, 0);						
					eventController.FireOptionalEvent(purchaseNewFactoryEvent, continent);
				}
			}
		
			if (continent.factories.Count > 0)
			{
				if (Random.Range(1, 100000) < 5)
				{
					environmentalDisasterEvent = new Event.GameEvent.EnvironmentalDisasterEvent(continent);
					eventController.FireOptionalEvent(environmentalDisasterEvent, continent);
				}
			}
		
			int count = 0;
			foreach(Factory factory in continent.factories)
			{	
				if (factory.workHours > 10)
				{
					if (Random.Range(1, 100000) < 10)
					{
						workerSuicideEvent = new Event.GameEvent.WorkerSuicideEvent(continent, count);
						eventController.FireOptionalEvent(workerSuicideEvent, continent);
					}
				
					if (Random.Range(1, 100000) < 10)
					{
						workerProtestEvent = new Event.GameEvent.WorkerProtestEvent(count);
						eventController.FireEvent(workerProtestEvent, continent);
					}
				}
				count++;
			}
		}
		
		foreach(Continent continent in world.GetComponent<World>().continents)
		{
			if(continent.wealthiness == "Impoverished" || continent.wealthiness == "Poor" && continent.factories.Count > 0)
			{
				if(Random.Range (1, 300000) < 2)
				{
					internationalInvestigationEvent = new Event.GameEvent.InternationalInvestigationEvent(continent.name);
					eventController.FireOptionalEvent(internationalInvestigationEvent, continent);
				}
			}
		}

		if(lastYearIncome == (Company.companyMoneyIN - Company.companyMoneyOUT) && thisYear == TimeManager.TIME_ELAPSED_IN_MONTHS - 12)
		{
			idleCompanyEvent = new Event.GameEvent.IdleCompanyEvent();
			eventController.FireOptionalEvent(idleCompanyEvent);

			thisYear = TimeManager.TIME_ELAPSED_IN_MONTHS;
		}
		else if(lastYearIncome != (Company.companyMoneyIN - Company.companyMoneyOUT))
		{
			thisYear = TimeManager.TIME_ELAPSED_IN_MONTHS;
		}

		lastYearIncome = (Company.companyMoneyIN - Company.companyMoneyOUT);

		if (thisYear < TimeManager.TIME_ELAPSED_IN_MONTHS - 12)
			thisYear = TimeManager.TIME_ELAPSED_IN_MONTHS - 12;
	}
}

