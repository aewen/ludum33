﻿using UnityEngine;
using System.Collections;

public class TimeManager : MonoBehaviour {

	public static int TIME_ELAPSED_IN_MONTHS = 0;
	public static int TIME_ELAPSED_IN_YEARS = 0;

	float totalFrameTimer = 0.0f;

	float secondsPerMonth = 5.0f;

	bool yearsUpdated = false;

	// Use this for initialization
	void Start () {
		TIME_ELAPSED_IN_MONTHS = 0;
		TIME_ELAPSED_IN_YEARS = 1;

		totalFrameTimer = 0.0f;

		secondsPerMonth = 5.0f;

		yearsUpdated = false;
	}
	
	// Update is called once per frame
	void Update () {

		totalFrameTimer += Time.deltaTime;

		if(totalFrameTimer >= secondsPerMonth)
		{
			TIME_ELAPSED_IN_MONTHS++;

			totalFrameTimer = 0.0f;
		}

		if(TIME_ELAPSED_IN_MONTHS % 12 == 0 && TIME_ELAPSED_IN_MONTHS != 0 && !yearsUpdated)
		{
			TIME_ELAPSED_IN_YEARS++;

			yearsUpdated = true;
		}
		else if(TIME_ELAPSED_IN_MONTHS % 12 != 0 && yearsUpdated)
		{
			yearsUpdated = false;
		}

		//Debug.Log (totalFrameTimer);
		//Debug.Log ("Months: " + (TIME_ELAPSED_IN_MONTHS));
		//Debug.Log ("Years: " + TIME_ELAPSED_IN_YEARS);
	}
}
